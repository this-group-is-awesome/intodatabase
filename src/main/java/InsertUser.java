
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class InsertUser {

    public static void main(String[] args) {
        Connection conn = null;
        String dbName = "user.db";

        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");

            conn = DriverManager.getConnection("jdbc:sqlite:" + dbName);
            conn.setAutoCommit(false);

            stmt = conn.createStatement();
            String sql = "";

            stmt.executeUpdate(sql);

            conn.commit();

            stmt.close();

            conn.close();
        } catch (ClassNotFoundException ex) {
            System.out.println("/!\\Library org.sqlite.JDBC not found!");
            System.exit(0);
        } catch (SQLException ex) {
            System.out.println("/!\\Unable to open database!");
            System.exit(0);
        }

        System.out.println("Open database success");
    }
}
